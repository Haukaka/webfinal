package com.example.DAO;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class RoleDao extends JdbcDaoSupport {

    @Autowired
    public RoleDao(DataSource dataSource) {
        this.setDataSource(dataSource);
    }
 
    public List<String> getRoleNames(Integer idUser) {
        String sql = "Select r.Role_Name " //
                + " from User_Role ur, Role r " //
                + " where ur.Role_Id = r.Id and ur.Id_User = ? ";
 
        Object[] params = new Object[] { idUser };
 
        List<String> roles = this.getJdbcTemplate().queryForList(sql, params, String.class);
 
        return roles;
    }
}

package com.example.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.example.entity.Images;

@RestController
public class DownloadController {

	@GetMapping(value = "/download")
	public StreamingResponseBody getSteamingFile(HttpServletRequest request, HttpServletResponse response, Images img)
			throws IOException {
		try {
			String test = "/WEB-INF/images/user-1.jpg";
			ServletContext cntx = request.getServletContext();
			System.out.println(cntx.getContextPath());
			String fileName = cntx.getRealPath(test);
			String mime = cntx.getMimeType(fileName);
			response.setContentType(mime);
			response.setHeader("Content-Disposition", "attachment; filename=\"test.jpg\"");
			InputStream inputStream = new FileInputStream(new File(fileName));
			return outputStream -> {
				int nRead;
				byte[] data = new byte[1024];
				while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
					outputStream.write(data, 0, nRead);
				}
			};
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}

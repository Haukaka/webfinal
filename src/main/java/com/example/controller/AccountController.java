package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.entity.Users;
import com.example.service.UserService;
import com.example.validation.UsersValidator;

@RestController
public class AccountController {

	@Autowired
	UserService usersService;

	@Autowired
	private UsersValidator usersValidator;

	@GetMapping(value = "/login")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView("login/login");
		return mav;
	}
	@GetMapping(value = "/index")
	public ModelAndView index() {
		ModelAndView mav = new ModelAndView("user/index");
		return mav;
	}
	@PostMapping(value = "/xulydangnhap")
	public ModelAndView loginPage() {
		ModelAndView mav = new ModelAndView("user/index");
		return mav;
	}

	@RequestMapping(value = "/register" , method = RequestMethod.GET)
	public ModelAndView register(@ModelAttribute("myForm2") Users myForm) {
		ModelAndView mav = new ModelAndView("login/register");
		return mav;
	}

	@PostMapping("/registration")
	public ModelAndView registration(@ModelAttribute("myForm2") Users myForm, BindingResult bindingResult) {
		usersValidator.validate(myForm, bindingResult);

		if (bindingResult.hasErrors()) {
			ModelAndView mav = new ModelAndView("login/register");
			return mav;
		}
		usersService.save(myForm);
		ModelAndView mav = new ModelAndView("login/login");
		return mav;
	}

	@GetMapping(value = "/forgotPass")
	public ModelAndView forgotPass() {
		ModelAndView mav = new ModelAndView("login/forgotPass");
		return mav;
	}

	@GetMapping(value = "/confirm_register")
	public ModelAndView confirm_register() {
		ModelAndView mav = new ModelAndView("login/confirm_register");
		return mav;
	}

	@GetMapping(value = "/confirm")
	public ModelAndView confirm() {
		ModelAndView mav = new ModelAndView("login/confirm");
		return mav;
	}

	@GetMapping(value = "/changePassword")
	public ModelAndView changePassword() {
		ModelAndView mav = new ModelAndView("login/changePassword");
		return mav;
	}

}

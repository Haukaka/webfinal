package com.example.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.example.entity.Categories;
import com.example.entity.Images;
import com.example.entity.Users;
import com.example.repository.CategoryRepository;
import com.example.repository.ImagesRepository;
import com.example.service.CategoryService;
import com.example.service.ImagesService;
//import com.example.service.CategoriesService;

@RestController
public class HomeController {

	@Autowired
	private CategoryRepository categoryReponsitory;
	@Autowired
	private ImagesRepository imagesRepository;

	@Autowired
	private ImagesService imagesService;

	@Autowired
	private CategoryService categoryService;

	@GetMapping(value = "/")
	public ModelAndView homePage() {
		ModelAndView mav = new ModelAndView("user/index");
		return mav;
	}

	@RequestMapping(value = "/gallery")
	public ModelAndView aboutPage() {
		ModelAndView mav = new ModelAndView("user/gallery");
		List<Images> images = imagesRepository.findAll();
		if (images.isEmpty()) {
			Images i1 = new Images(1, "hau", "tot", "img/sidebar-img/1.jpg", 1, 2, 10, null, null, null);
			Images i2 = new Images(2, "ha", "kem", "img/sidebar-img/2.jpg", 2, 4, 11, null, null, null);
			Images i3 = new Images(3, "ngoc", "vua", "img/sidebar-img/3.jpg", 3, 5, 18, null, null, null);
			Images i4 = new Images(4, "dan", "tot", "img/sidebar-img/4.jpg", 4, 8, 89, null, null, null);
			Images i5 = new Images(5, "huy", "kem", "img/sidebar-img/5.jpg", 5, 9, 34, null, null, null);
			List<Images> listImages = new ArrayList<Images>();
			listImages.add(i1);
			listImages.add(i2);
			listImages.add(i3);
			listImages.add(i4);
			listImages.add(i5);
			imagesRepository.saveAll(listImages);

		}
		List<Images> dataImages = imagesRepository.findAll();
		mav.addObject("gallery", dataImages);
		return mav;
	}

	@RequestMapping(value = "/categories")
	public ModelAndView categoriesPage() {
		ModelAndView mav = new ModelAndView("user/categories");
		List<Categories> categories = categoryReponsitory.findAll();

		if (categories.isEmpty()) {
			Categories c1 = new Categories(1, "people", "img/sidebar-img/1.jpg");
			Categories c2 = new Categories(2, "animal", "img/sidebar-img/2.jpg");
			Categories c3 = new Categories(3, "nature", "img/sidebar-img/3.jpg");
			Categories c4 = new Categories(4, "food", "img/sidebar-img/4.jpg");
			Categories c5 = new Categories(5, "art", "img/sidebar-img/5.jpg");
			Categories c6 = new Categories(6, "structure", "img/sidebar-img/6.jpg");
			List<Categories> listCategories = new ArrayList<Categories>();
			listCategories.add(c1);
			listCategories.add(c2);
			listCategories.add(c3);
			listCategories.add(c4);
			listCategories.add(c5);
			listCategories.add(c6);
			categoryReponsitory.saveAll(listCategories);

		}
		List<Categories> dataCategory = categoryReponsitory.findAll();
		mav.addObject("categories", dataCategory);

		return mav;

	}

	@RequestMapping(value = "/contact")
	public ModelAndView contactPage() {
		ModelAndView mav = new ModelAndView("user/contact");
		return mav;
	}

	// test view
	@RequestMapping(value = "/galleryDetail")
	public ModelAndView testPage(Model model, @RequestParam(value = "idImg") Integer idImg) {
		ModelAndView mav = new ModelAndView("user/galleryDetail");
		model.addAttribute("images", imagesService.findByIdImg(idImg));
		return mav;
	}

	@GetMapping(value = "/listImg")
	public ModelAndView listImg(@RequestParam(value = "idCategory") int idCategory) {
		ModelAndView mav = new ModelAndView("user/gallery");
		List<Images> list = imagesService.findByIdCategory(idCategory);
		mav.addObject("gallery", list);
		return mav;
	}
	
	
}

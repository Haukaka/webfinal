package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entity.Categories;
import com.example.entity.Images;

public interface ImagesRepository extends JpaRepository<Images, Integer> {
	Images findByIdImg(int idImg);

	List<Images> findByidCategory(Categories cat);
}
  
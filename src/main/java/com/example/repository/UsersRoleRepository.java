package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.entity.User_Roles;
import com.example.entity.Users;
@Repository
public interface UsersRoleRepository extends JpaRepository<User_Roles, Integer> {
	User_Roles findByUsers(Users users);
}

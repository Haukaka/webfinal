package com.example.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Categories")
public class Categories {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idCategory", nullable = false)
	private int idCategory;

	@Column(name = "nameCategory")
	private String nameCategory;

	@Column(name = "imgCategory")
	private String imgCategory;

	@OneToMany(mappedBy = "idCategory", cascade = CascadeType.ALL)
	private List<Images> images;
	public Categories() {
		super();
	}

	public Categories(int idCategory, String nameCategory, String imgCategory) {
		super();
		this.idCategory = idCategory;
		this.nameCategory = nameCategory;
		this.imgCategory = imgCategory;
	}
	
	

	public Categories(String nameCategory) {
		super();
		this.nameCategory = nameCategory;
	}

	public int getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}

	public String getNameCategory() {
		return nameCategory;
	}

	public void setNameCategory(String nameCategory) {
		this.nameCategory = nameCategory;
	}

	public String getImgCategory() {
		return imgCategory;
	}

	public void setImgCategory(String imgCategory) {
		this.imgCategory = imgCategory;
	}

	public List<Images> getImages() {
		return images;
	}

	public void setImages(List<Images> images) {
		this.images = images;
	}

}

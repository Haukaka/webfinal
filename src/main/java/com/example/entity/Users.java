package com.example.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
//entity
@Entity
@Table(name = "Users")
public class Users {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idUser", nullable = false)
	private int idUser;

	@Column(name = "userName")
	private String userName;

	@Column(name = "userImg")
	private String userImg;

	@Column(name = "password")
	private String password;
	

    @Transient
    private String passwordConfirm;

	public String getPasswordConfirm() {
		return passwordConfirm;
	}


	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}


	@Column(name = "email")
	private String email;

	@Column(name = "sex")
	private String sex;

	@Column(name = "address")
	private String address;

	@OneToMany(mappedBy = "idUser", cascade = CascadeType.ALL)
	private List<Images> images;

	public Users() {
		super();
	}

	public Users(String userName) {
		super();
		this.userName = userName;
	}

	public Users(int idUser, String userName, String userImg, String password, String email, String sex, String address,
			List<Images> images) {
		super();
		this.idUser = idUser;
		this.userName = userName;
		this.userImg = userImg;
		this.password = password;
		this.email = email;
		this.sex = sex;
		this.address = address;
		this.images = images;
	}

}

package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Images")
public class Images {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idImg", nullable = false)
	private int idImg;

	@Column(name = "imgName")
	private String imgName;

	@Column(name = "quanlity")
	private String quanlity;

	@Column(name = "img")
	private String img;

	@Column(name = "likes")
	private int likes;

	@Column(name = "downloads")
	private int downloads;

	@Column(name = "comments")
	private int comments;

	@ManyToOne
	@JoinColumn(name = "idCategory")
	private Categories idCategory;

	@ManyToOne
	@JoinColumn(name = "idUser")
	private Users idUser;

	public Images() {
		super();
	}
	

	public Images(int idImg, String imgName, String quanlity, String img, int likes, int downloads, int comments,
			Categories idCategory, Users idUser, byte[] pic) {
		super();
		this.idImg = idImg;
		this.imgName = imgName;
		this.quanlity = quanlity;
		this.img = img;
		this.likes = likes;
		this.downloads = downloads;
		this.comments = comments;
		this.idCategory = idCategory;
		this.idUser = idUser;
		this.pic = pic;
	}

	public int getIdImg() {
		return idImg;
	}

	public void setIdImg(int idImg) {
		this.idImg = idImg;
	}

	public String getImgName() {
		return imgName;
	}

	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	public String getQuanlity() {
		return quanlity;
	}

	public void setQuanlity(String quanlity) {
		this.quanlity = quanlity;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getDownloads() {
		return downloads;
	}

	public void setDownloads(int downloads) {
		this.downloads = downloads;
	}

	public int getComments() {
		return comments;
	}

	public void setComments(int comments) {
		this.comments = comments;
	}

	public Categories getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Categories idCategory) {
		this.idCategory = idCategory;
	}

	public Users getIdUser() {
		return idUser;
	}

	public void setIdUser(Users idUser) {
		this.idUser = idUser;
	}

	public byte[] getPic() {
		return pic;
	}

	public void setPic(byte[] pic) {
		this.pic = pic;
	}

	public Images(int idImg, String imgName, String quanlity, String img, int likes, int downloads, int comments,
			Date postDay, Categories idCategory, Users idUser) {
		super();
		this.idImg = idImg;
		this.imgName = imgName;
		this.quanlity = quanlity;
		this.img = img;
		this.likes = likes;
		this.downloads = downloads;
		this.comments = comments;
		this.postDay = postDay;
		this.idCategory = idCategory;
		this.idUser = idUser;
	}

	public Images(String img, int likes, int downloads, int comments, Date postDay, Users idUser) {
		super();
		this.img = img;
		this.likes = likes;
		this.downloads = downloads;
		this.comments = comments;
		this.postDay = postDay;
		this.idUser = idUser;
	}

	public int getIdImg() {
		return idImg;
	}

	public void setIdImg(int idImg) {
		this.idImg = idImg;
	}

	public String getImgName() {
		return imgName;
	}

	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	public String getQuanlity() {
		return quanlity;
	}

	public void setQuanlity(String quanlity) {
		this.quanlity = quanlity;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getDownloads() {
		return downloads;
	}

	public void setDownloads(int downloads) {
		this.downloads = downloads;
	}

	public int getComments() {
		return comments;
	}

	public void setComments(int comments) {
		this.comments = comments;
	}

	public Date getPostDay() {
		return postDay;
	}

	public void setPostDay(Date postDay) {
		this.postDay = postDay;
	}

	public Categories getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Categories idCategory) {
		this.idCategory = idCategory;
	}

	public Users getIdUser() {
		return idUser;
	}

	public void setIdUser(Users idUser) {
		this.idUser = idUser;
	}

}

package com.example.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordUtils {
	   public static String password(String password) {
	        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	        return encoder.encode(password);
	    }
	 
	    public static void main(String[] args) {
	        String password = "123";
	        String testPassword = password(password);
	 
	        System.out.println("Password: " + testPassword);
	    }
}

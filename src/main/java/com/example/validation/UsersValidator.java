package com.example.validation;

import org.dom4j.util.UserDataAttribute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.example.entity.Users;
import com.example.repository.UsersRepository;
import com.example.service.UserDetailsServiceImpl;
import com.example.service.UserService;

@Component
public class UsersValidator implements Validator {

	@Autowired
	private UsersRepository usersRepository;

	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;

	@Override
	public boolean supports(Class<?> aClass) {
		return User.class.equals(aClass);
	}


    @Override
    public void validate(Object o, Errors errors) {
        Users users = (Users) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");
        if (!users.getEmail().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
            errors.rejectValue("email", "Size.myForm.email");
        }
        if (users.getUserName().length() < 3 || users.getUserName().length() > 32) {
            errors.rejectValue("userName", "Size.myForm.userName");
        }
        if( usersRepository.findByEmail(users.getEmail()) != null) {
            errors.rejectValue("email", "Duplicate.myForm.email");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (users.getPassword().length() < 8 || users.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.myForm.password");
        }

        if (!users.getPasswordConfirm().equals(users.getPassword())) {
            errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
        }
    }
}

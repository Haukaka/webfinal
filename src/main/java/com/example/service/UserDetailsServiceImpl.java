package com.example.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.entity.Role;
import com.example.entity.User_Roles;
import com.example.entity.Users;
import com.example.repository.RoleRepository;
import com.example.repository.UsersRepository;
import com.example.repository.UsersRoleRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	private UsersRepository userReponsitory;

	@Autowired
	private UsersRoleRepository usersRoleReponsitory;

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Users users = userReponsitory.findByEmail(email);
		System.out.println(email);

		if (users == null) {
			System.out.println("User not found! " + email);
			throw new UsernameNotFoundException("User " + email + " was not found in the database");
		}

		System.out.println("Found User: " + users);
		User_Roles userRoles = usersRoleReponsitory.findByUsers(users);
		System.out.println(userRoles);
		// [ROLE_USER, ROLE_ADMIN,..]
		Role roleNames = roleRepository.findById(userRoles.getRole().getId());

		List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
		GrantedAuthority authority = new SimpleGrantedAuthority(roleNames.getRoleName());
		System.out.println(authority);
		grantList.add(authority);
		System.out.println("------------->");
		System.out.println(grantList);
		UserDetails userDetails = (UserDetails) new User(users.getUserName(), //
				users.getPassword(), grantList);
		System.out.println(userDetails);
		return userDetails;
	}
}

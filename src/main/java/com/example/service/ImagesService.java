package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.Categories;
import com.example.entity.Images;
import com.example.repository.ImagesRepository;

@Service
public class ImagesService {
	@Autowired
	private ImagesRepository imagesRepository;

	@Autowired
	private CategoryService categoryService;

	public Images findByIdImg(int idImg) {
		Images img = imagesRepository.findByIdImg(idImg);
		return img;

	}

	public List<Images> findByIdCategory(int idCategory) {
		Categories category = categoryService.findById(idCategory);
		List<Images> img = imagesRepository.findByidCategory(category);
		return img;
	}

}

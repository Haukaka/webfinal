package com.example.service;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.entity.Role;
import com.example.entity.User_Roles;
import com.example.entity.Users;
import com.example.repository.RoleRepository;
import com.example.repository.UsersRepository;
import com.example.repository.UsersRoleRepository;

@Service
public class UserService {

	@Autowired
	private UsersRepository userRepository;

	public List<Users> findAll() {

		return null;
	}
	@Autowired
	private RoleRepository roleResponsitory;
	@Autowired
	private UsersRoleRepository usersRoleResponsitory;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public void save(Users users) {
		users.setPassword(bCryptPasswordEncoder.encode(users.getPassword()));
		
		userRepository.save(users);
		Role role = roleResponsitory.findById(1);
		User_Roles userRole= new User_Roles(role, users);
		usersRoleResponsitory.save(userRole);
	}
	

}

<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Title -->
<title>H3N2M Photos</title>

<!-- Favicon -->
<link rel="icon" href="img/core-img/favicon.ico">

<!-- Core Stylesheet -->
<link href="style.css" rel="stylesheet">


<!-- Responsive CSS -->
<link href="css/responsive/responsive.css" rel="stylesheet">

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style type="text/css">
	.owl-stage-outer img{
		width: 80px;
		height: 310px;
	}
	
	body{
		background-attachment: fixed;
    	background-position: center center;
    	background-image: url(img/bg-img/bg-pattern.jpg);
    	position: relative;
    	background-size: cover;
    	z-index: 1;
        backdrop-filter: blur(3px);
	}
</style>

</head>


<body>

	<%@ include file="/WEB-INF/fragment/header.jsp"%>

	<!-- ****** Welcome Post Area Start ****** -->
	<section class="welcome-post-sliders owl-carousel" style="margin-bottom: 30px;">

		<!-- Single Slide -->
		<div class="welcome-single-slide">
			<!-- Post Thumb -->
			<img src="img/product-img/1.jpg" alt="">
			<!-- Overlay Text -->
			<div class="project_title">
				<div class="post-date-commnents d-flex">
					<a href="#">May 19, 2017</a> <a href="#">5 Comment</a>
				</div>
				<a href="#">
					<h5>“I’ve Come and I’m Gone”: A Tribute to Istanbul’s Street</h5>
				</a>
			</div>
		</div>

		<!-- Single Slide -->
		<div class="welcome-single-slide">
			<!-- Post Thumb -->
			<img src="img/product-img/2.jpg" alt="">
			<!-- Overlay Text -->
			<div class="project_title">
				<div class="post-date-commnents d-flex">
					<a href="#">May 19, 2017</a> <a href="#">5 Comment</a>
				</div>
				<a href="#">
					<h5>“I’ve Come and I’m Gone”: A Tribute to Istanbul’s Street</h5>
				</a>
			</div>
		</div>

		<!-- Single Slide -->
		<div class="welcome-single-slide">
			<!-- Post Thumb -->
			<img src="img/product-img/3.jpg" alt="">
			<!-- Overlay Text -->
			<div class="project_title">
				<div class="post-date-commnents d-flex">
					<a href="#">May 19, 2017</a> <a href="#">5 Comment</a>
				</div>
				<a href="#">
					<h5>“I’ve Come and I’m Gone”: A Tribute to Istanbul’s Street</h5>
				</a>
			</div>
		</div>

		<!-- Single Slide -->
		<div class="welcome-single-slide">
			<!-- Post Thumb -->
			<img src="img/product-img/4.jpg" alt="">
			<!-- Overlay Text -->
			<div class="project_title">
				<div class="post-date-commnents d-flex">
					<a href="#">May 19, 2017</a> <a href="#">5 Comment</a>
				</div>
				<a href="#">
					<h5>“I’ve Come and I’m Gone”: A Tribute to Istanbul’s Street</h5>
				</a>
			</div>
		</div>

		<!-- Single Slide -->
		<div class="welcome-single-slide">
			<!-- Post Thumb -->
			<img src="img/product-img/5.jpg" alt="">
			<!-- Overlay Text -->
			<div class="project_title">
				<div class="post-date-commnents d-flex">
					<a href="#">May 19, 2017</a> <a href="#">5 Comment</a>
				</div>
				<a href="#">
					<h5>“I’ve Come and I’m Gone”: A Tribute to Istanbul’s Street</h5>
				</a>
			</div>
		</div>

	</section>
	<!-- ****** Blog Area End ****** -->

	<!-- ****** Our Creative Portfolio Area End ****** -->


	<!-- Jquery-2.2.4 js -->
	<script src="js/jquery/jquery-2.2.4.min.js"></script>
	<!-- Popper js -->
	<script src="js/bootstrap/popper.min.js"></script>
	<!-- Bootstrap-4 js -->
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<!-- All Plugins JS -->
	<script src="js/others/plugins.js"></script>
	<!-- Active JS -->
	<script src="js/active.js"></script>


	<%@ include file="/WEB-INF/fragment/footer.jsp"%>
</body>
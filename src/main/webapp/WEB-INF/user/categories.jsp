<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Title -->
<title>H3N2M Photos</title>

<!-- Favicon -->
<link rel="icon" href="img/core-img/favicon.ico">

<!-- Core Stylesheet -->
<link href="style.css" rel="stylesheet">

<!-- Responsive CSS -->
<link href="css/responsive/responsive.css" rel="stylesheet">

<style type="text/css">
	body{
		background-attachment: fixed;
    	background-position: center center;
    	background-image: url(img/bg-img/bg-pattern.jpg);
    	position: relative;
    	background-size: cover;
    	z-index: 1;
        backdrop-filter: blur(3px);
	}
</style>
</head>

<body>

	<%@ include file="/WEB-INF/fragment/header.jsp"%>
	<div class="breadcumb-area"
		style="background-image: url(img/bg-img/breadcumb.jpg);">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div class="col-12">
					<div class="bradcumb-title text-center">
						<h2>Categories Page</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="breadcumb-nav">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#"><i
									class="fa fa-home" aria-hidden="true"></i> Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Categories
								Page</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>

	<!-- ****** Categories Area Start ****** -->
	<section class="categories_area clearfix" id="about">
		<div class="container">
			<div class="row">
				<c:forEach items="${categories}" var="cat">
					<div class="col-12 col-md-6 col-lg-4">
						<div class="single_catagory wow fadeInUp" data-wow-delay=".3s">
							<img src="${cat.getImgCategory()}" alt="animal">
							<div class="catagory-title">
								<a href="/listImg?idCategory=${cat.idCategory }">
									<h5>${cat.nameCategory}</h5>
								</a>
							</div>
						</div>
					</div>

				</c:forEach>

			</div>
		</div>
	</section>

	<!-- ****** Categories Area End ****** -->

	<!-- ****** Instagram Area Start ****** -->
	<!-- ****** Our Creative Portfolio Area End ****** -->
	<div>
		<p></p>
	</div>
	<%@ include file="/WEB-INF/fragment/footer.jsp"%>

	<!-- Jquery-2.2.4 js -->
	<script src="js/jquery/jquery-2.2.4.min.js"></script>
	<!-- Popper js -->
	<script src="js/bootstrap/popper.min.js"></script>
	<!-- Bootstrap-4 js -->
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<!-- All Plugins JS -->
	<script src="js/others/plugins.js"></script>
	<!-- Active JS -->
	<script src="js/active.js"></script>
</body>
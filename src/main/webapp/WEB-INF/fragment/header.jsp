<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="users" scope="session" value="${pageContext.request.userPrincipal.name}"></c:set>
<!-- Favicon -->
<link rel="icon" href="img/core-img/favicon.ico">

<!-- Core Stylesheet -->
<link href="style.css" rel="stylesheet">


<!-- Responsive CSS -->
<link href="css/responsive/responsive.css" rel="stylesheet">

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style type="text/css">
td {
	text-align: right;
	padding-left: 90px;
}

table {
	margin-left: 20px;
}

.download {
	border-top: 1px solid #ebebeb;
	padding-top: 0px;
}

.btndown {
	border: 1px solid #fc6c3f;
	border-radius: 20px;
	background: #fc6c3f;
	color: #fff;
	width: 400px;
	padding: 2px 15px 2px 15px;
	text-align: left;
}

.btndown:hover {
	border: 1px solid #fc6c3f;
	border-radius: 20px;
	background: #fff;
	color: #fc6c3f;
}
</style>


<!-- ****** Top Header Area Start ****** -->
<div class="top_header_area">
	<div class="container">
		<div class="row">

			<div class="col-5 col-sm-12">
				<!--  Top Social bar start -->
				<c:choose>
					<c:when test="${empty users }">
						<div style="padding-left: 900px;" class="top_social_bar">
							<a href="${pageContext.request.contextPath}/login">Login</a>| <a
								href="${pageContext.request.contextPath}/register">Register</a>
						</div>
					</c:when>
					<c:otherwise>
						<div></div>
						<div style="padding-left: 900px;" class="top_social_bar">
							<div>
								Welcome<a href="${pageContext.request.contextPath}/admin">
									${users}</a>
							</div>
							| <a href="${pageContext.request.contextPath}/logout">Logout</a>
						</div>

					</c:otherwise>
				</c:choose>
			</div>

		</div>
	</div>
</div>
<!-- ****** Top Header Area End ****** -->

<!-- ****** Header Area Start ****** -->
<header class="header_area">
	<div class="container">
		<div class="row">
			<!-- Logo Area Start -->
			<div class="col-12">
				<div class="logo_area text-center">
					<a href="${pageContext.request.contextPath}/" class="yummy-logo">H3N2M
						Photos</a>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<nav class="navbar navbar-expand-lg">
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#yummyfood-nav" aria-controls="yummyfood-nav"
						aria-expanded="false" aria-label="Toggle navigation">
						<i class="fa fa-bars" aria-hidden="true"></i> Menu
					</button>
					<!-- Menu Area Start -->
					<div class="collapse navbar-collapse justify-content-center"
						id="yummyfood-nav">
						<ul class="navbar-nav" id="yummy-nav">
							<li class="nav-item active"><a class="nav-link"
								href="${pageContext.request.contextPath}/">Home <span
									class="sr-only">(current)</span></a></li>
							<li class="nav-item"><a class="nav-link"
								href="${pageContext.request.contextPath}/gallery">Gallery</a></li>
							<li class="nav-item"><a class="nav-link"
								href="${pageContext.request.contextPath}/categories">Categories</a></li>
							<li class="nav-item"><a class="nav-link"
								href="${pageContext.request.contextPath}/contact">Contact</a></li>



						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</header>
<!-- ****** Header Area End ****** -->
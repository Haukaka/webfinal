
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Change Password</title>

    <!-- Icons font CSS-->
<link href="/vendor/mdi-font/css/material-design-iconic-font.min.css"
	rel="stylesheet" media="all">
<link href="/vendor/font-awesome-4.7/css/font-awesome.min.css
	rel="stylesheet" media="all">
<!-- Font special for pages-->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Inconsolata&display=swap"
	rel="stylesheet">

<!-- Vendor CSS-->
<link href="/vendor/select2/select2.min.css" rel="stylesheet" media="all">
<link href="/vendor/datepicker/daterangepicker.css" rel="stylesheet"
	media="all">
<!-- Main CSS-->
<link href="/css/main.css" rel="stylesheet" media="all">

    
    <style type="text/css">
    	.linkform{
    		text-decoration: none;
    		padding-left: 253px;
    		color: #84b4cb;s
    	}
    	.linkform:hover{
    		color: #a02c2d;
    	}
		.forgotpass, .changepass{
			text-decoration: none;
    		color: #a02c2d;
		}
		.forgotpass:hover, .changepass:hover{
			color: #84b4cb;
		}
		.changepass{
			padding-left: 220px;
		}
    </style>
 
</head>
<script>
        $(document).ready(function() {
            
            $('#myForm').bind({
                'submit': function() {
                   
                    if ($('#newpass').val()=='') {
                        $('#error_newpass').html('Bạn chưa nhập mật khẩu');
                        return false;
                    }
                    if ($('#newpass').val()!=$('#renewpass').val()) {
                        $('#error_renewpass').html('Mật khẩu không trùng');
                        return false;
                    }
                   
                   
												
												return true;
											},
											'keydown' : function() {
												if ($('#newpass').val().length > 0) {
													$('#error_newpass')
															.html('');
												}
												if ($('#newpass').val()!=$('#renewpass').val()) {
													$('#error_renewpass')
															.html('');
												}
												
											}
										});
					});
</script>
<body>

    <div class="page-wrapper bg-red p-t-180 p-b-100 font-robo">
        <div class="wrapper wrapper--w960">
            <div class="card card-2">
                <div class="card-heading"></div>
                <div class="card-body">
                    <h2 class="title">Change Password</h2>
                    <!-- <p style="margin-top: 10px; margin-bottom: 10px; color: #a02c2d;">Use the form below to change your password. 
                    			Your password cannot be the same as your username.</p> -->
                    <form method="POST" id="myForm">
                      
                        <div style="display: none">
												<input type="password" class="form-control" id="action"
													name="action" value="doimatkhau" onkeydown="">
											</div> 
                        <div class="input-group">
                            <input class="input--style-2" type="password" placeholder="New password" name="newpass" id="newpass">
                            <p id="error_newpass" style="color: red;"></p>
                        </div>
                         <div class="input-group">
                            <input class="input--style-2" type="password" placeholder="Confirm password" name="renewpass" id="renewpass">
                            <p id="error_renewpass" style="color: red;"></p>
                        </div>
                        <div class="p-t-30">
                            <button class="btn btn--radius btn--green" type="submit">Change password</button>
                            <!-- <a class="linkform" href="../index.jsp">Cancel</a> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
	<script src="/vendor/jquery/jquery.min.js"></script>
	<!-- Vendor JS-->
	<script src="/vendor/select2/select2.min.js"></script>
	<script src="/vendor/datepicker/moment.min.js"></script>
	<script src="/vendor/datepicker/daterangepicker.js"></script>

	<!-- Main JS-->
	<script src="/js/global.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->